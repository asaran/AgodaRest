package com.amar.agodaRest.models;


import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

import com.amar.agodaRest.utils.CommonUtils;



@Component("appDataHolder")
public class AppDataHolder implements InitializingBean {
	private List<Hotel> hotels;
	private ConcurrentMap<String,RateLimiterData> rateLimitors;
	public void setHotals() {
		
	}
	public ConcurrentMap<String, RateLimiterData> getRateLimitors() {
		return rateLimitors;
	}
	
	public void setApiKeysRateLimitters(ConcurrentMap<String, RateLimiterData> rateLimitors) {
		this.rateLimitors = rateLimitors;
	}
	public List<Hotel> getHotels() {
		return hotels;
	}
	public void setHotels(List<Hotel> hotels) {
		this.hotels = hotels;
	}
	
	@Override
	public void afterPropertiesSet() throws Exception {
		// TODO Auto-generated method stub
		rateLimitors = new ConcurrentHashMap<>();
		hotels = CommonUtils.getHotelsFromCSV();

	}
	
}
