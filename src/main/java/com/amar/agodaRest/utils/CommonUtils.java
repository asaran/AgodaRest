package com.amar.agodaRest.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.amar.agodaRest.models.Hotel;

import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.bean.CsvToBean;
import au.com.bytecode.opencsv.bean.HeaderColumnNameTranslateMappingStrategy;


public class CommonUtils {
	
	public static List<Hotel> getHotelsFromCSV() {
		List<Hotel> hotels = null;
		List<Hotel> hotelsWithCityId = new ArrayList<Hotel>();
		HeaderColumnNameTranslateMappingStrategy<Hotel> beanStrategy = new HeaderColumnNameTranslateMappingStrategy<Hotel>();
		beanStrategy.setType(Hotel.class);
		
		Map<String, String> columnMapping = new HashMap<String, String>();
		columnMapping.put("CITY", "city");
		columnMapping.put("HOTELID", "hotelId");
		columnMapping.put("ROOM", "room");
		columnMapping.put("PRICE", "price");
		beanStrategy.setColumnMapping(columnMapping);
		CsvToBean<Hotel> csvToBean = new CsvToBean<Hotel>();
	
		
		File f = new File("hoteldb.csv");
		try (FileReader fileReader = new FileReader(f.getAbsolutePath())) {
			CSVReader reader = new CSVReader(fileReader, ',');
			hotels = csvToBean.parse(beanStrategy, reader);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
				
		for (Hotel hotel : hotels) {
			hotel.setCityId(hotel.getCity().hashCode());//Assuming that same city (String) has same hashCode @Todo check
			hotelsWithCityId.add(hotel);
		}
		
		return hotelsWithCityId;
	}


}
