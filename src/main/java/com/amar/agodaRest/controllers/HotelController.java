package com.amar.agodaRest.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.amar.agodaRest.models.ResponseJson;

@RestController
public class HotelController {
	
	@RequestMapping(value="/",method=RequestMethod.GET)
		public ResponseEntity<?> welcome() {
		return ResponseEntity.ok().body("Wellcome to agoda assignment by Amar.");
	}
	//,consumes=MediaType.APPLICATION_JSON_VALUE
	
	@RequestMapping(value="/apikey",method=RequestMethod.POST,
			produces=MediaType.APPLICATION_JSON_VALUE)
		public ResponseEntity<ResponseJson> createApiKey() {
		ResponseJson responseJson = new ResponseJson();
		ResponseJson rj = new ResponseJson();
		rj.setData("amar hello").setMessage("success msg");
		
		responseJson.
		setData(rj).
		setMessage("Success").
		setStatusCode(HttpStatus.OK.value());
		return ResponseEntity.status(HttpStatus.OK).body(responseJson);
		
	}

}
